package com.amadeus.privateuniversity.helloacslight;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloAcsLightApplication {

	public static void main( String[] args ) {
		SpringApplication.run( HelloAcsLightApplication.class, args );
	}

}

